import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IEmployee } from '@shared/models/clockifyUsers.model';

@Component({
  selector: 's-bill',
  templateUrl: './s-bill.component.html',
  styleUrls: ['./s-bill.component.scss'],
})
export class BillComponent {
  @Input() public summaryReportArray: Array<IEmployee>;
  @Input() public bill: Array<IEmployee>;
  @Input() public searchText: string;

  @Output() public pushToBill: EventEmitter<void> = new EventEmitter();
  @Output() public sendBill: EventEmitter<void> = new EventEmitter();

  public addToBill(item: IEmployee): void {
    const index = this.summaryReportArray.findIndex(
      x => x._id == item._id,
    );
    this.bill.push(this.summaryReportArray[index]);
    this.summaryReportArray.splice(index, 1);
  }

  public removeFromBill(item: IEmployee): void {
    const index = this.bill.findIndex(
      x => x._id == item._id,
    );
    this.summaryReportArray.push(this.bill[index]);
    this.bill.splice(index, 1);
  }

  public filterCondition(item: IEmployee) {
    return item.name.toLowerCase().indexOf(this.searchText.toLowerCase()) != -1;
  }
}
