import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpRequestsService } from '@services/http-requests.service';
import { OrderModalComponent } from '@shared/components/dialogs/order-modal/order-modal.component';

@Component({
  selector: 'v-bill',
  templateUrl: './view-bill.component.html',
})
export class ViewBillComponent implements OnInit {
  public bill = [];
  public searchText = '';
  public clockifyUsers: Object;

  constructor(
    public readonly httpRequests: HttpRequestsService,
    public readonly http: HttpClient,
    public readonly dialog: MatDialog,
  ) { }

  public ngOnInit(): void {
    this.httpRequests.getClockifyUsersSummaryReports$()
      .subscribe(response => {
        response.forEach(u => {
          delete u.totals;
        });
        for (const key in response) {
          for (const key2 in response[key]) {
            this.clockifyUsers = response[key][key2];
          }
        }
      });
  }

  public pushToBill(): void {
    const employeesList = this.clockifyUsers;
    this.bill.push(employeesList[0]);
  }

  public sendBill(): void {
    this.dialog.open(OrderModalComponent, {
      data: {
        message: this.bill,
      },
    });
  }
}
