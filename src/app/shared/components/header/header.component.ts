import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  public invoicelyUrl = 'https://invoicely.com';
  public clockifyUrl = 'https://clockify.me/tracker';
}
