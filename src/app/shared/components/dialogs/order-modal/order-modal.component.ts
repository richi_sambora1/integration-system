import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'order-modal',
  templateUrl: './order-modal.component.html',
  styleUrls: ['./order-modal.component.scss'],
})
export class OrderModalComponent {
  public message;
  public selectedValue: object = [''];
  public vendorsToBill: object = ['', 'EPAM', 'AMAZON', 'IBM', 'Nekit Brat'];
  public employeesData: Object[];
  public buttonDisabled: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<OrderModalComponent>,
  ) {
    if (data) {
      this.message = data.message || this.message;
    }
  }

  public onSelectedChange(value: string): void {
    this.employeesData = this.message.forEach(function (element) {
      element.vendor = value;
    });
  }

  public sendData(): void {
    console.log(this.message);
  }

  public onChange($event: Event) {
    this.buttonDisabled = true;
  }
}
