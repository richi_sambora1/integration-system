import { OrderModalComponent } from '@shared/components/dialogs/order-modal/order-modal.component';

export const Modals = [
  OrderModalComponent,
];
