export interface IEmployee {
  _id: number;
  name: string;
  duration: number;
  amount: number;
}
