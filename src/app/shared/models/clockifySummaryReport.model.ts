export interface ISummaryReport {
  totals: {
    _id: string;
    totalTime: number;
    totalBillableTime: number;
    entriesCount: number;
    totalAmount: number;
  };
  groupOne: {
    _id: number;
    name: string;
    duration: number;
    amount: number;
    nameLowerCase: string;
    children: any;
  };
}
