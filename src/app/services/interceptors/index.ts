import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BackendInterceptorService } from '@services/interceptors/backend-interceptor.service';
import { TokenInterceptorService } from '@services/interceptors/token-interceptor.service';

export const Interceptors = [
  { provide: HTTP_INTERCEPTORS, useClass: BackendInterceptorService, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true },
];
