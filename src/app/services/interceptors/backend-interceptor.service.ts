import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

const clockifySummaryReport: Array<any> = [
  {
    'totals': [
      {
        '_id': '',
        'totalTime': 458118,
        'totalBillableTime': 458118,
        'entriesCount': 33,
        'totalAmount': 7.9459,
      },
    ],
    'groupOne': [
      {
        '_id': '5e2feaa6b128ac31f2589da2',
        'name': 'Nikita Sufranovich',
        'duration': 6528,
        'nameLowerCase': 'nikita sufranovich',
        'amount': 2555.25,
        'children': null,
      },
      {
        '_id': '5d4451b5d278ae57b51d7dd0',
        'name': 'Kurinartyr',
        'duration': 451,
        'nameLowerCase': 'kurinartyr',
        'amount': 7.945,
        'children': null,
      },
      {
        '_id': '5d4451b5d278ae57b51d7dd1',
        'name': 'Delen Alex',
        'duration': 213,
        'nameLowerCase': 'Delen Alex',
        'amount': 12,
        'children': null,
      },
      {
        '_id': '5d4451b5d278ae57b51d7dd2',
        'name': 'Sergey Prokopof',
        'duration': 312,
        'nameLowerCase': 'Sergey Prokopof',
        'amount': 15,
        'children': null,
      },
      {
        '_id': '5d4451b5d278ae57b51d7dd3',
        'name': 'Anton Pankov',
        'duration': 1234,
        'nameLowerCase': 'Anton Pankov',
        'amount': 8,
        'children': null,
      },
    ],
  },
];

export const InterceptorSkipHeader = 'Backend-Skip-Interceptor';

@Injectable({
  providedIn: 'root',
})
export class BackendInterceptorService implements HttpInterceptor {
  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.headers.has(InterceptorSkipHeader)) {
      const headers = req.headers.delete(InterceptorSkipHeader);

      return next.handle(req.clone({headers}));
    }
    if (req.method === 'GET' && req.url === 'https://reports.api.clockify.me/v1/workspaces/5e2feaa6b128ac31f2589da3/reports/summary') {
      return of(new HttpResponse({status: 200, body: clockifySummaryReport}));
    }
    next.handle(req);
  }
}
