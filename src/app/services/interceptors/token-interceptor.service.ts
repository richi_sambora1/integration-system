import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export const InterceptorSkipHeader = 'Clockify-Token-Skip-Interceptor';

@Injectable({
  providedIn: 'root',
})

export class TokenInterceptorService implements HttpInterceptor {
  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.headers.has(InterceptorSkipHeader)) {
      const headers = req.headers.delete(InterceptorSkipHeader);

      return next.handle(req.clone({ headers }));
    }

    const tokenReq = req.clone({
      headers: req.headers.set('X-Api-Key', 'XqFsMGEDYW5iT9FW'),
    });

    return next.handle(tokenReq);
  }
}
