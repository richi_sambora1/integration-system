import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpRequestsService {
  public clockifySummaryReportUrl = 'https://reports.api.clockify.me/v1/workspaces/5e2feaa6b128ac31f2589da3/reports/summary';

  constructor(
    public readonly http: HttpClient,
  ) { }

  public getClockifyUsersSummaryReports$(): Observable<any> {
    return this.http.get<any>(this.clockifySummaryReportUrl)
      .pipe(data => {
        return data;
      });
  }
}
