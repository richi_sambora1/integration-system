import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { Pages } from '@pages/index';
import { Services } from '@services/index';
import { Interceptors } from '@services/interceptors';
import { Modals } from '@shared/components/dialogs';
import { MaterialModule } from '@shared/material.module';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OrderModalComponent } from './shared/components/dialogs/order-modal/order-modal.component';
import { HeaderComponent } from './shared/components/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    Pages,
    HeaderComponent,
    OrderModalComponent,
  ],
  entryComponents: [
    Modals,
  ],
  imports: [
    MaterialModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatDialogModule,
    MatSelectModule,
  ],
  providers: [
    Services,
    Interceptors,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
