import * as puppeteer from 'puppeteer';

// const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setDefaultNavigationTimeout(0);

  const userName = 'lorinif451@j24blog.com';
  const urlName = 'berkut';
  const password = 'detroit313';

  const invoiceData = {
    invoiceName: 'smth',
    invoiceDescr: 'Burda perda',
  };
  console.log('start...');

  await page.goto('https://invoicely.com/login');
  const emailInput = await page.$('#email');
  await emailInput.type(userName);

  const passwordInput = await page.$('#password');
  await passwordInput.type(password);

  await page.click('#root > div > div.css-1vq466o > div > div.css-1rhsm9q > div > form > div.css-1l4w6pd > button');
  await page.waitForSelector('#root > div > div > div > div.css-1yu5v1 > div > div > div:nth-child(1) > h2');
  await page.click('#root > div > div > div > div.css-1yu5v1 > div > div > div:nth-child(2) > div > div');
  // await page.waitForSelector('#wrapper > nav > div.upper > a.logo');

  await page.screenshot({path: 'invoicely_login.png'});

  await page.goto(`https://${urlName}.invoicely.com/invoices/new`, {
    waitUntil: 'load',
    timeout: 0,
  });
  // invoice title
  const titleInput = await page.$('#new_invoice > div > div.board.front > div > div > section > header > h2 > input[type=text]');
  await titleInput.click({clickCount: 3});
  await titleInput.type('Hey mama', {delay: 20});
  // invoice descr
  const descriptInput = await page.$('#new_invoice > div > div.board.front > div > div > section > header > div.form_row.statement_description > textarea');
  await descriptInput.type('some description about invoice');
  // invoice item
  const itemTextArea = await page.$('#item_textarea_1 > textarea');
  await itemTextArea.type('some description about item');
  // invoice quantity
  const quantityInput = await page.$('#drag_item_list > li > ul > li.item_column.quantity > div > div > input[type=text]');
  await quantityInput.type('30');
  // invoice rate
  const rateInput = await page.$('#drag_item_list > li > ul > li.item_column.amount > div > div > input');
  await rateInput.type('12');
  // invoice amount
  const amountInput = await page.$('#drag_item_list > li > ul > li.item_column.unit > div > div > input[type=text]');
  await amountInput.type('1111');

  const saveActionButton = await page.$('#wrapper > main > header > div > div > a');
  await saveActionButton.click();
  // await page.keyboard.press('Enter');
  // await page.keyboard.down('Enter');

  // const saveButton = await page.$('#wrapper > main > header > div > div > ul > li:nth-child(1) > button');
  // await saveButton.click();

  await page.screenshot({path: 'invoicely_newInvoice.png'});
  console.log('...finish');
  await browser.close();
})();


